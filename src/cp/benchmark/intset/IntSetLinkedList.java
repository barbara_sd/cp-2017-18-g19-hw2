package cp.benchmark.intset;

public class IntSetLinkedList implements IntSet {

  public class Node {
    private final int m_value;
    private Node m_next;

    public Node(int value, Node next) {
      m_value = value;
      m_next = next;
    }

    public Node(int value) {
      this(value, null);
    }

    public int getValue() {
      return m_value;
    }

    public void setNext(Node next) {
      m_next = next;
    }

    public Node getNext() {
      return m_next;
    }
  }

  private final Node m_first;

  private int produced_counter;
  private int consumed_counter;

  public IntSetLinkedList() {
    Node min = new Node(Integer.MIN_VALUE);
    Node max = new Node(Integer.MAX_VALUE);
    this.produced_counter = 2;
    this.consumed_counter = 0;
    min.setNext(max);
    m_first = min;
  }

  public boolean add(int value) {
    boolean result;

    Node previous = m_first;
    Node next = previous.getNext();
    int v;
    while ((v = next.getValue()) < value) {
      previous = next;
      next = previous.getNext();
    }
    result = v != value;
    if (result) {
      previous.setNext(new Node(value, next));
      produced_counter++;
    }
    return result;
  }

  public boolean remove(int value) {
    boolean result;

    Node previous = m_first;
    Node next = previous.getNext();
    int v;
    while ((v = next.getValue()) < value) {
      previous = next;
      next = previous.getNext();
    }
    result = v == value;
    if (result) {
      previous.setNext(next.getNext());
      consumed_counter++;
    }
    return result;
  }

  public boolean contains(int value) {
    boolean result;

    Node previous = m_first;
    Node next = previous.getNext();
    int v;
    while ((v = next.getValue()) < value) {
      previous = next;
      next = previous.getNext();
    }
    result = (v == value);

    return result;
  }

  public void validate() {
    java.util.Set<Integer> checker = new java.util.HashSet<>();
    int previous_value = m_first.getValue();
    Node node = m_first.getNext();
    int value = node.getValue();

    while (value < Integer.MAX_VALUE) {
      assert previous_value < value : "list is unordered: " + previous_value + " before " + value;
      assert !checker.contains(value) : "list has duplicates: " + value;
      checker.add(value);
      previous_value = value;
      node = node.getNext();
      value = node.getValue();
    }
    assert produced_counter >= consumed_counter : "there must be more enough produced items to be consumed";
    assert consumed_counter >= 0 : "Items to be consumed must be greater os equal to zero" + consumed_counter;
    assert produced_counter -256 < (consumed_counter + checker.size()):  "there are items not consumed " + checker.size();
  }
}
