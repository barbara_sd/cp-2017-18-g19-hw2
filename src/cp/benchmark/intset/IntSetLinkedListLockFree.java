package cp.benchmark.intset;

import java.util.concurrent.atomic.AtomicMarkableReference;

public class IntSetLinkedListLockFree implements IntSet {

	public class Node {
		private final int m_value;
		private AtomicMarkableReference<Node> m_next;

		public Node(final int value, final Node next) {
			this.m_value = value;
			this.m_next = new AtomicMarkableReference<Node>(next, false);
		}

		public Node(int value) {
			this(value, null);
		}

		public int getValue() {
			return m_value;
		}

		public void setNext(Node next) {
			m_next.set(next,false);
		}

		public Node getNext() {
			return m_next.getReference();
		}
	}

	private final Node m_first;

	private int produced_counter;
	private int consumed_counter;

	public IntSetLinkedListLockFree() {
		Node min = new Node(Integer.MIN_VALUE);
		Node max = new Node(Integer.MAX_VALUE);
		this.produced_counter = 2;
		this.consumed_counter = 0;
		min.setNext(max);
		m_first = min;
	}

	public class Window{
		public Node previous;
		public Node current;
		Window(Node previous, Node current){
			this.previous = previous;
			this.current = current;
		}
	}

	public boolean add(int value) {
		while(true){
			Window window = find(m_first, value);
			Node previous = window.previous;
			Node current = window.current;
			if(current.getValue() == value){
				return false;
			}else{
				Node node = new Node(value);
				node.m_next = new AtomicMarkableReference(current, false);
				if(previous.m_next.compareAndSet(current, node, false, false)){
					produced_counter++;
					return true;
				}
			}
		}
	}

	public boolean remove(int value) {
		boolean snip = false;
		while(true){
			Window window = find(m_first, value);
			Node previous = window.previous;
			Node current = window.current;
			if(current.getValue() != value){
				return false;
			}
			else{
				Node successor = current.m_next.getReference();
				snip = current.m_next.attemptMark(successor, true);
				if(!snip){
					continue;
				}
				previous.m_next.compareAndSet(current, successor, false, false);
				consumed_counter++;
				return true;
			}
		}
	}

	public boolean contains(int value) {
		boolean[] marked = {false};
		Node current = m_first;
		while(current.getValue() < value){
			current = current.getNext();
			Node successor = current.m_next.get(marked);
		}
		return (current.getValue() == value && !marked[0]);
	}

	public void validate() {
		java.util.Set<Integer> checker = new java.util.HashSet<>();
		int previous_value = m_first.getValue();
		Node node = m_first.getNext();
		int value = node.getValue();
		while (value < Integer.MAX_VALUE) {
			assert previous_value < value : "list is unordered: " + previous_value + " before " + value;
			assert !checker.contains(value) : "list has duplicates: " + value;
			checker.add(value);
			previous_value = value;
			node = node.getNext();
			value = node.getValue();
		}
//		assert produced_counter >= consumed_counter : "there must be more enough produced items to be consumed";
		assert consumed_counter >= 0 : "Items to be consumed must be greater os equal to zero" + consumed_counter;
		assert produced_counter -256 < (consumed_counter + checker.size()):  "there are items not consumed " + checker.size();

	}

	public Window find(Node head, int value){
		Node previous = null;
		Node current = null;
		Node successor = null;
		boolean[] marked = {false};
		boolean snip = false;
		retry: while(true){
			previous = head;
			current = previous.m_next.getReference();
			while(true){
				successor = current.m_next.get(marked);
				while(marked[0]){
					snip = previous.m_next.compareAndSet(current, successor,false, false);
					if(!snip){
						continue retry;
					}
					current = successor;
					successor = current.m_next.get(marked);
				}
				if(current.getValue() >= value){
					return new Window(previous, current);
				}
				previous = current;
				current = successor;
			}
		}
	}
}
