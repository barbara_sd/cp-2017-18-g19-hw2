package cp.benchmark.intset;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class IntSetLinkedListOptimisticPerNodeLock implements IntSet {

	public class Node {
		private final int m_value;
		private Node m_next;
		private Lock lock;

		public Node(int value, Node next) {
			m_value = value;
			m_next = next;
			lock = new ReentrantLock();
		}

		public Node(int value) {
			this(value, null);
		}

		public int getValue() {
			return m_value;
		}

		public void setNext(Node next) {
			m_next = next;
		}

		public Node getNext() {
			return m_next;
		}

		public void lock() {
			lock.lock();
		}

		public void unlock() {
			lock.unlock();
		}

	}

	private final Node m_first;

	private int produced_counter;
	private int consumed_counter;

	public IntSetLinkedListOptimisticPerNodeLock() {
		Node min = new Node(Integer.MIN_VALUE);
		Node max = new Node(Integer.MAX_VALUE);
	    this.produced_counter = 2;
	    this.consumed_counter = 0;
		min.setNext(max);
		m_first = min;
	}

	public boolean add(int value) {
		while(true) {
			Node previous = m_first;
			Node current = previous.getNext();
			while(current.getValue() < value) {
				previous = current;
				current = current.getNext();
			}
			previous.lock();
			current.lock();
			try {
				if(validation(previous, current)) {
					if(current.getValue() == value) {
						return false;
					} else {
						Node node = new Node(value);
						node.setNext(current);
						previous.setNext(node);
					  produced_counter++;
						return true;
					}
				}
			} finally {
				previous.unlock();
				current.unlock();
			}
		}

	}

	public boolean remove(int value) {
		while(true) {
			Node previous = m_first;
			Node current = previous.getNext();
			while(current.getValue() < value) {
				previous = current;
				current = current.getNext();
			}
			previous.lock();
			current.lock();
			try {
				if(validation(previous, current)) {
					if(current.getValue() == value) {
						previous.setNext(current.getNext());
						consumed_counter++;
						return true;
					}
					else {
						return false;
					}
				}
			} finally {
				previous.unlock();
				current.unlock();
			}
		}
	}

	public boolean contains(int value) {
		while(true) {
			Node previous = m_first;
			Node current = previous.getNext();
			while(current.getValue() < value) {
				previous = current;
				current = current.getNext();
			}
			try {
				previous.lock();
				current.lock();
				if(validation(previous, current)) {
					return (current.getValue() == value);
				}
			} finally {
				previous.unlock();
				current.unlock();
			}
		}
	}

	private boolean validation(Node previous, Node current) {
		Node node = m_first;
		while(node.getValue() <= previous.getValue()) {
			if(node == previous) {
				return previous.getNext() == current;
			}
			node = node.getNext();
		}
		return false;
	}

	public void validate() {
		java.util.Set<Integer> checker = new java.util.HashSet<>();
		int previous_value = m_first.getValue();
		Node node = m_first.getNext();
		int value = node.getValue();
		while (value < Integer.MAX_VALUE) {
			assert previous_value < value : "list is unordered: " + previous_value + " before " + value;
			assert !checker.contains(value) : "list has duplicates: " + value;
			checker.add(value);
			previous_value = value;
			node = node.getNext();
			value = node.getValue();
		}
	//			assert produced_counter >= consumed_counter : "there must be more enough produced items to be consumed" + produced_counter + " " + consumed_counter;
			  assert consumed_counter >= 0 : "Items to be consumed must be greater os equal to zero" + consumed_counter;
			  assert (produced_counter - 256) < (consumed_counter + checker.size()):  "there are items not consumed" + checker.size()+ " " +produced_counter + " " + consumed_counter;

	}

}
