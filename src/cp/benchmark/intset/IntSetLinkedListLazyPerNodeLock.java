package cp.benchmark.intset;

import java.util.concurrent.locks.*;

public class IntSetLinkedListLazyPerNodeLock implements IntSet {

	public class Node {
		private final int m_value;
		private Node m_next;
		private Lock lock;
		private boolean marked;

		public Node(int value, Node next) {
			m_value = value;
			m_next = next;
			lock = new ReentrantLock();
			marked = false;
		}

		public Node(int value) {
			this(value, null);
		}

		public int getValue() {
			return m_value;
		}

		public void setNext(Node next) {
			m_next = next;
		}

		public Node getNext() {
			return m_next;
		}

		public void lock() {
			lock.lock();
		}

		public void unlock() {
			lock.unlock();
		}

		public void setMarked(boolean value){
			marked = value;
		}

		public boolean getMarked(){
			return marked;
		}
	}

	private final Node m_first;

	private int produced_counter;
	private int consumed_counter;

	public IntSetLinkedListLazyPerNodeLock() {
		Node min = new Node(Integer.MIN_VALUE);
		Node max = new Node(Integer.MAX_VALUE);
		this.produced_counter = 2;
		this.consumed_counter = 0;
		min.setNext(max);
		m_first = min;
	}

	public boolean add(int value) {
		while(true){
			Node previous = m_first;
			Node current = m_first.getNext();
			while(current.getValue() < value){
				previous = current;
				current = current.getNext();
			}
			previous.lock();
			try{
				current.lock();
				try{
					if(validation(previous,current)){
						if(current.getValue() == value){
							return false;
						}else{
							Node node = new Node(value);
							node.setNext(current);
							previous.setNext(node);
							produced_counter++;
							return true;
						}
					}
				}finally{
					current.unlock();
				}
			}finally{
				previous.unlock();
			}
		}
	}

	public boolean remove(int value) {
		while(true){
			Node previous = m_first;
			Node current = m_first.getNext();
			while(current.getValue() < value){
				previous = current;
				current = current.getNext();
			}
			previous.lock();
			try{
				current.lock();
				try{
					if(validation(previous,current)){
						if(current.getValue() != value){
							return false;
						}else{
							current.setMarked(true);
							previous.setNext(current.getNext());
							consumed_counter++;
							return true;
						}
					}
				}finally{
					current.unlock();
				}
			}finally{
				previous.unlock();
			}
		}
	}

	public boolean contains(int value) {
		Node current = m_first;
		while(current.getValue() < value){
			current = current.getNext();
		}
		return (current.getValue() == value && !current.getMarked());
	}

	private boolean validation(Node previous, Node current) {
		return (!previous.getMarked() && !current.getMarked() && previous.getNext() == current);
	}

	public void validate() {
		java.util.Set<Integer> checker = new java.util.HashSet<>();
		int previous_value = m_first.getValue();
		Node node = m_first.getNext();
		int value = node.getValue();
		while (value < Integer.MAX_VALUE) {
			assert previous_value < value : "list is unordered: " + previous_value + " before " + value;
			assert !checker.contains(value) : "list has duplicates: " + value;
			checker.add(value);
			previous_value = value;
			node = node.getNext();
			value = node.getValue();
		}
//		assert produced_counter >= consumed_counter : "there must be more enough produced items to be consumed";
		assert consumed_counter >= 0 : "Items to be consumed must be greater os equal to zero" + consumed_counter;
		assert produced_counter -256 < (consumed_counter + checker.size()):  "there are items not consumed " + checker.size();

	}
}
